#! /bin/bash
#### VARIABLES ####
COLORSCHEME="doom-one"

lxsession &
picom &
dwmblocks &
/usr/bin/emacs --daemon &
killall conky && conky -c "$HOME"/.config/conky/bspwm/"$COLORSCHEME"-01.conkyrc

### Uncomment only ONE of the following ###
# uncomment this line to restore last saved wallpaper...
xargs xwallpaper --stretch < ~/.cache/wall &
# ...or uncomment this line to set a random wallpaper on login
# find /usr/share/backgrounds/dtos-backgrounds/ -type f | shuf -n 1 | xargs xwallpaper --stretch &
